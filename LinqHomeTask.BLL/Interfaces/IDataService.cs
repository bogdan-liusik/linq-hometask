﻿using System.Collections.Generic;
using System.Threading.Tasks;
using LinqHomeTask.Common.Entities;

namespace LinqHomeTask.BLL.Interfaces
{
    public interface IDataService
    {
        Task<List<Project>> GetDataHierarchyAsync();
    }
}