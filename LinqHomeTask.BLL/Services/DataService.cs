﻿using System;
using System.Linq;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;
using LinqHomeTask.BLL.Interfaces;
using LinqHomeTask.Common.Entities;
using System.Threading.Tasks;
using Task = LinqHomeTask.Common.Entities.Task;

namespace LinqHomeTask.BLL.Services
{
    public class DataService : IDataService
    {
        private readonly HttpClient _httpClient;
        private const string BaseAddress = "https://bsa21.azurewebsites.net/api/";

        public DataService()
        {
            _httpClient = new HttpClient()
            {
                BaseAddress = new Uri(BaseAddress)
            };
        }
        
        public async Task<List<Project>> GetDataHierarchyAsync()
        {
            var users = await FetchDataFromApiAsync<User>("users");
            var teams = await FetchDataFromApiAsync<Team>("teams");
            var tasks = await FetchDataFromApiAsync<Task>("tasks");
            var projects = await FetchDataFromApiAsync<Project>("projects");
            
            teams.ForEach(t => t.Performers = users.Where(u => u.TeamId == t.Id).ToList());
            users.ForEach(u => u.Tasks = tasks.Where(t => t.PerformerId == u.Id).ToList());
            users.ForEach(u => u.Projects = projects.Where(p => p.AuthorId == u.Id).ToList());
            tasks.ForEach(t => t.Project = projects.FirstOrDefault(p => p.Id == t.ProjectId));
            users.ForEach(u => u.Team = teams.FirstOrDefault(t => t.Id == u.TeamId));
            
            var hierarchy = (from project in projects
                join task in tasks on project.Id equals task.ProjectId into tasksGroup
                join author in users on project.AuthorId equals author.Id
                join team in teams on project.TeamId equals team.Id
                select new Project()
                {
                    Id = project.Id,
                    AuthorId = project.AuthorId,
                    TeamId = project.TeamId,
                    Name = project.Name,
                    Description = project.Description,
                    Deadline = project.Deadline,
                    CreatedAt = project.CreatedAt,
                    Author = users.FirstOrDefault(u => u.Id == project.AuthorId),
                    Team = team,
                    Tasks = (from task in tasksGroup select task).ToList()
                }).ToList();;
                
            return hierarchy;
        }
        
        private async Task<List<T>> FetchDataFromApiAsync<T>(string endpoint) where T: class
        {
            return JsonConvert.DeserializeObject<List<T>>(await _httpClient.GetStringAsync(endpoint));
        }
    }
}