﻿using System;
using System.Collections.Generic;
using System.Linq;
using LinqHomeTask.Common.Entities;
using LinqHomeTask.Common.Entities.Enums;

namespace LinqHomeTask.BLL.Services
{
    public class QueryService
    {
        private readonly List<Project> _data;
        
        public QueryService(List<Project> data)
        {
            _data = data;
        }
        
        public Dictionary<Project, int> Query1(int userId)
        {
            return _data
                .Where(p => p.AuthorId == userId).ToList()
                .ToDictionary(
                    p => p,
                    p => p.Tasks.Count);
        }

        public List<Task> Query2(int userId)
        {
            return _data
                .SelectMany(p => p.Tasks)
                .Where(t => t.Description != null && t.PerformerId == userId && t.Description.Length < 45)
                .ToList();
        }

        public List<(int taskId, string taskName)> Query3(int userId)
        {
            var currentYear = DateTime.Now.Year;
            return _data
                .SelectMany(p => p.Tasks)
                .Where(t => t.FinishedAt != null && t.PerformerId == userId && t.FinishedAt.Value.Year == currentYear)
                .Select(t =>
                (
                    taskId: t.Id,
                    taskName: t.Name
                ))
                .ToList();
        }

        public List<(int Id, string Name, List<User> Users)> Query4()
        {
            var currentYear = DateTime.Now.Year;
            
            return _data
                .Select(p => p.Team).Distinct()
                .Select(t =>
                (
                    Id: t.Id,
                    Name: t.Name,
                    Users: t.Performers
                        .Where(u => u.BirthDay.Year < currentYear - 10)
                        .OrderByDescending(u => u.RegisteredAt)
                        .ToList()
                ))
                .ToList();
        }

        public List<User> Query5()
        {
            return _data
                .SelectMany(p => p.Team.Performers).Distinct()
                .Select(u => new User()
                {
                    Id = u.Id,
                    TeamId = u.TeamId,
                    FirstName = u.FirstName,
                    LastName = u.LastName,
                    Projects = u.Projects,
                    Team = u.Team,
                    Email = u.Email,
                    RegisteredAt = u.RegisteredAt,
                    BirthDay = u.BirthDay,
                    Tasks = u.Tasks.OrderByDescending(t => t.Name.Length).ToList()
                }).ToList()
                .OrderBy(u => u.FirstName).ToList()
                .ToList();
        }
        
        public (User User, Project LastUserProject, int NumberOfTasksInLastProject, int IncompleteOrCalcelled, Task LongestTaskByDate) Query6(int userId)
        {
            var user = _data
                .SelectMany(p => p.Team.Performers)
                .FirstOrDefault(u => u.Id == userId);
            
            return 
                (
                    User: user, 
                    LastUserProject: user.Projects.OrderBy(p => p.CreatedAt).LastOrDefault(), 
                    NumberOfTasksInLastProject: user.Projects.OrderBy(p => p.CreatedAt).LastOrDefault().Tasks.Count,
                    IncompleteOrCalcelled: user.Tasks.Count(t => t.FinishedAt == null || t.State == TaskState.Canceled),
                    LongestTaskByDate: user.Tasks.OrderBy(t => t.FinishedAt != null ? t.FinishedAt - t.CreatedAt : DateTime.Now - t.CreatedAt).LastOrDefault()
                );
        }

        public List<(Project Project, Task LongestProjectTaskByDescription, Task ShortestProjectTaskByName, int UsersLenghtByCondition)> Query7()
        {
            return _data.Select(p => (
                Project: p,
                LongestProjectTaskByDescription: p.Tasks
                    .OrderBy(t => t.Description.Length).LastOrDefault(),
                ShortestProjectTaskByName: p.Tasks
                    .OrderBy(t => t.Name.Length).FirstOrDefault(),
                UsersLenghtByCondition: p.Description != null && (p.Description.Length > 20 || p.Tasks.Count < 3) ? p.Team.Performers.ToList().Count : 0
            )).ToList();
        }
    }
}