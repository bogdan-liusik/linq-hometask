﻿namespace LinqHomeTask.Common.Entities.Enums
{
    public enum TaskState
    {
        ToDo = 0,
        InProgress,
        Done,
        Canceled
    }
}