﻿using System;
using System.Collections.Generic;

namespace LinqHomeTask.Common.Entities
{
    public class Team
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public DateTime CreatedAt { get; set; }
        public List<User> Performers { get; set; }
    }
}