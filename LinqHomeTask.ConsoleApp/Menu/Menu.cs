﻿using LinqHomeTask.BLL.Services;

namespace LinqHomeTask.ConsoleApp.Menu
{
    using System;
    using MenuHandlers;
    using System.Collections.Generic;
    using static MenuHelper;
    
    public class Menu
    {
        private bool _running = true;
        readonly Dictionary<int, MenuHandler> _handlers;

        public Menu(QueryService queryService)
        {
            _handlers = new Dictionary<int, MenuHandler>()
            {
                {1, new Query1Handler(queryService)},
                {2, new Query2Handler(queryService)},
                {3, new Query3Handler(queryService)},
                {4, new Query4Handler(queryService)},
                {5, new Query5Handler(queryService)},
                {6, new Query6Handler(queryService)},
                {7, new Query7Handler(queryService)},
            };
        }
        
        public void Start()
        {
            while (_running)
            {
                ShowCommands();
                ChooseCommand();
            }
        }
        
        private void ShowCommands()
        {
            WriteColorLine("┌" + new string('─', 13) + "QUERY SERVICE MENU" + new string('─', 11) + "┐", ConsoleColor.Green);
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("| 1. Result of Query 1                     |");
            Console.WriteLine("| 2. Result of Query 2                     |");
            Console.WriteLine("| 3. Result of Query 3                     |");
            Console.WriteLine("| 4. Result of Query 4                     |");
            Console.WriteLine("| 5. Result of Query 5                     |");
            Console.WriteLine("| 6. Result of Query 6                     |");
            Console.WriteLine("| 7. Result of Query 7                     |");
            WriteColorLine("| 0. Exit                                  |", ConsoleColor.Red);
            WriteColorLine("└" + new string('─', 42) + "┘", ConsoleColor.Green);
        }

        private void ChooseCommand()
        {
            try
            {
                Console.Write("Enter your choice: ");
                
                if(Int32.TryParse(Console.ReadLine(), out var menuItem))
                {
                    if (menuItem is <= 7 and > 0)
                    {
                        _handlers[menuItem].Handle();
                    }
                    else if(menuItem == 0)
                    {
                        _running = false;
                    }
                    else
                    {
                        throw new InvalidOperationException("Item number must be in range 1 - 7! Try Again.");
                    }
                }
                else
                {
                    throw new InvalidOperationException("Invalid character! Try Again.");
                }
            }
            catch (InvalidOperationException ex)
            {
                WriteErrorLine(ex.Message);
            }

            WriteColorLine("\n" + new string('─', 81) + "\n", ConsoleColor.DarkGray);
        }
    }
}