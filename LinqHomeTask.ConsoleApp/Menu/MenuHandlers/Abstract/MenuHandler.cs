﻿using LinqHomeTask.BLL.Services;

namespace LinqHomeTask.ConsoleApp.Menu.MenuHandlers
{
    public abstract class MenuHandler
    {
        readonly QueryService _queryService;

        public MenuHandler(QueryService queryService)
        {
            _queryService = queryService;
        }
        
        public QueryService QueryService => _queryService;

        public abstract void Handle();
    }
}