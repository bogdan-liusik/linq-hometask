﻿using System;
using LinqHomeTask.BLL.Services;
using static LinqHomeTask.ConsoleApp.Menu.MenuHelper;

namespace LinqHomeTask.ConsoleApp.Menu.MenuHandlers
{
    class Query1Handler : MenuHandler
    {
        public Query1Handler(QueryService queryService) : base(queryService) { }

        public override void Handle()
        {
            try
            {
                WriteColorLine("Task:", ConsoleColor.Green);
                WriteColorLine("Get the number of tasks of a particular user's project (by id) (dictionary, where the key is the project, and the value is the number of tasks).", ConsoleColor.Yellow);
                
                Console.WriteLine("Enter user id (for example 65): ");
                string id = Console.ReadLine();
                
                var result = QueryService.Query1(Int32.Parse(id));

                if (result.Count == 0)
                {
                    Console.WriteLine("This user has no projects of his own.");
                    WriteEnterAnyKeyToContinue();
                    return;
                }
                
                WriteColorLine("Result: ", ConsoleColor.Yellow);
                foreach (var keyValuePair in result)
                {
                    WriteSuccessLine($"ProjectID: {keyValuePair.Key.Id} | ProjectName: {keyValuePair.Key.Name} | Number of tasks: {keyValuePair.Value}");
                }
                
                WriteEnterAnyKeyToContinue();
            }
            catch (Exception exception)
            {
                WriteErrorLine("Some error...");
                WriteErrorLine(exception.Message);
            }
        }
    }
}