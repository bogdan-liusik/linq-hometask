﻿using System;
using System.Threading.Channels;
using LinqHomeTask.BLL.Services;
using static LinqHomeTask.ConsoleApp.Menu.MenuHelper;

namespace LinqHomeTask.ConsoleApp.Menu.MenuHandlers
{
    class Query2Handler : MenuHandler
    {
        public Query2Handler(QueryService queryService) : base(queryService) { }

        public override void Handle()
        {
            try
            {
                WriteColorLine("Task:", ConsoleColor.Green);
                WriteColorLine("Get a list of tasks assigned to a specific user (by id), where name of task < 45 characters (collection of tasks).", ConsoleColor.Yellow);
                
                Console.WriteLine("Enter user id (for example 86): ");
                string id = Console.ReadLine();
                
                var result = QueryService.Query2(Int32.Parse(id));

                if (result.Count == 0)
                {
                    Console.WriteLine("This user has no tasks.");
                    WriteEnterAnyKeyToContinue();
                    return;
                }
                
                WriteColorLine("Result: ", ConsoleColor.Yellow);
                foreach (var task in result)
                {
                    WriteSuccessLine($"TaskID: {task.Id} | Name: {task.Name} | Description Length: {task.Description.Length}");
                }
                
                WriteEnterAnyKeyToContinue();
            }
            catch (Exception exception)
            {
                WriteErrorLine("Some error...");
                WriteErrorLine(exception.Message);
            }
        }
    }
}