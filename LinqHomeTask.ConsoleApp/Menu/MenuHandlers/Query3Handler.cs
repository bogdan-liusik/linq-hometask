﻿using System;
using LinqHomeTask.BLL.Services;
using LinqHomeTask.Common.Entities;
using static LinqHomeTask.ConsoleApp.Menu.MenuHelper;

namespace LinqHomeTask.ConsoleApp.Menu.MenuHandlers
{
    class Query3Handler : MenuHandler
    {
        public Query3Handler(QueryService queryService) : base(queryService) { }

        public override void Handle()
        {
            try
            {
                WriteColorLine("Task:", ConsoleColor.Green);
                WriteColorLine("Get the list (id, name) from the collection of tasks that are finished in the current year (2021) for a particular user (by id).", ConsoleColor.Yellow);
                
                Console.WriteLine("Enter user id (for example 35): ");
                string id = Console.ReadLine();
                
                var results = QueryService.Query3(Int32.Parse(id));

                if (results.Count == 0)
                {
                    Console.WriteLine("List is empty for current user.");
                    WriteEnterAnyKeyToContinue();
                    return;
                }
                
                WriteColorLine("Result: ", ConsoleColor.Yellow);
                foreach (var result in results)
                {
                    WriteSuccessLine($"ID: {result.taskId} | Name: {result.taskName}");
                }
                
                WriteEnterAnyKeyToContinue();
            }
            catch (Exception exception)
            {
                WriteErrorLine("Some error...");
                WriteErrorLine(exception.Message);
            }
        }
    }
}