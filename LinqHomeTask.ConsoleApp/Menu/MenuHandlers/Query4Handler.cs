﻿using System;
using LinqHomeTask.BLL.Services;
using static LinqHomeTask.ConsoleApp.Menu.MenuHelper;

namespace LinqHomeTask.ConsoleApp.Menu.MenuHandlers
{
    class Query4Handler : MenuHandler
    {
        public Query4Handler(QueryService queryService) : base(queryService) { }

        public override void Handle()
        {
            try
            {
                WriteColorLine("Task:", ConsoleColor.Green);
                WriteColorLine("Get a list (id, team name, and user list) of teams whose members are over 10 years old, sorted by user registration date in descending order, and grouped by team.", ConsoleColor.Yellow);

                var results = QueryService.Query4();
        
                if (results.Count == 0)
                {
                    Console.WriteLine("List is empty.");
                    WriteEnterAnyKeyToContinue();
                    return;
                }
                
                WriteColorLine("Result: ", ConsoleColor.Yellow);
                foreach (var result in results)
                {
                    WriteColorLine($"Command ID: {result.Id} | Command Name: {result.Name}", ConsoleColor.Cyan);
                    foreach (var user in result.Users)
                    {
                        WriteSuccessLine($"User ID: {user.Id} | First name {user.FirstName} | Team ID: {user.TeamId}");
                    }
                }
                WriteEnterAnyKeyToContinue();
            }
            catch (Exception exception)
            {
                WriteErrorLine("Some error...");
                WriteErrorLine(exception.Message);
            }
        }
    }
}