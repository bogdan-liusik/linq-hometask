﻿using System;
using LinqHomeTask.BLL.Services;
using static LinqHomeTask.ConsoleApp.Menu.MenuHelper;

namespace LinqHomeTask.ConsoleApp.Menu.MenuHandlers
{
    class Query5Handler : MenuHandler
    {
        public Query5Handler(QueryService queryService) : base(queryService) { }

        public override void Handle()
        {
            try
            {
                WriteColorLine("Task:", ConsoleColor.Green);
                WriteColorLine("Get a list of users alphabetically by first_name (ascending) with sorted tasks by name length (descending).", ConsoleColor.Yellow);

                var result = QueryService.Query5();
                
                if (result.Count == 0)
                {
                    Console.WriteLine("List is empty.");
                    WriteEnterAnyKeyToContinue();
                    return;
                }

                WriteColorLine("Result: ", ConsoleColor.Yellow);
                foreach (var user in result)
                {
                    WriteSuccessLine($"User ID: {user.Id} | First name {user.FirstName}");
                    WriteColorLine("User sorted tasks by name length:", ConsoleColor.Yellow);
                    foreach (var task in user.Tasks)
                    {
                        Console.WriteLine($"{task.Name}");
                    }
                }
                WriteEnterAnyKeyToContinue();
            }
            catch (Exception exception)
            {
                WriteErrorLine("Some error...");
                WriteErrorLine(exception.Message);
            }
        }
    }
}
