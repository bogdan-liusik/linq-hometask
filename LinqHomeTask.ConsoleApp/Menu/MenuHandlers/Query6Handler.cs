﻿using System;
using LinqHomeTask.BLL.Services;
using static LinqHomeTask.ConsoleApp.Menu.MenuHelper;

namespace LinqHomeTask.ConsoleApp.Menu.MenuHandlers
{
    class Query6Handler : MenuHandler
    {
        public Query6Handler(QueryService queryService) : base(queryService) { }

        public override void Handle()
        {
            try
            {
                WriteColorLine("Task:", ConsoleColor.Green);
                WriteColorLine("Get the following structure (pass user Id in parameters): \n" +
                               "User\n" +
                               "User's last project (by creation date)\n" +
                               "Total number of tasks under the last project\n" +
                               "Total number of incomplete or cancelled tasks for the user\n" +
                               "Longest user task by date (earliest created - latest finished)", ConsoleColor.Yellow);

                Console.WriteLine("Enter user id (for example 65): ");
                string id = Console.ReadLine();
                
                var result = QueryService.Query6(Int32.Parse(id));

                WriteColorLine("Result: ", ConsoleColor.Yellow);
                WriteSuccessLine($"User ID: {result.User.Id} | First Name: {result.User.FirstName}");
                WriteSuccessLine($"User's last project (by creation date): \n" +
                                 $"ProjectID: {result.LastUserProject.Id} | Name: {result.LastUserProject.Name}");
                WriteSuccessLine($"Total number of tasks under the last project: {result.NumberOfTasksInLastProject}");
                WriteSuccessLine($"Total number of incomplete or cancelled tasks for the user: {result.IncompleteOrCalcelled}");
                WriteSuccessLine($"Longest user task by date (earliest created - latest finished): \n" +
                                 $"Task ID: {result.LongestTaskByDate.Id} | Name: {result.LongestTaskByDate.Name}");
                WriteEnterAnyKeyToContinue();
            }
            catch (Exception exception)
            {
                WriteErrorLine("Some error...");
                WriteErrorLine(exception.Message);
                WriteColorLine("You can look at my request in QueryService, maybe he doesn't work properly :(", ConsoleColor.Yellow);
                WriteEnterAnyKeyToContinue();
            }
        }
    }
}