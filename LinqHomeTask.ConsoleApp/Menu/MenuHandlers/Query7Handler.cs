﻿using System;
using LinqHomeTask.BLL.Services;
using static LinqHomeTask.ConsoleApp.Menu.MenuHelper;

namespace LinqHomeTask.ConsoleApp.Menu.MenuHandlers
{
    class Query7Handler : MenuHandler
    {
        public Query7Handler(QueryService queryService) : base(queryService) { }

        public override void Handle()
        {
            try
            {
                WriteColorLine("Task:", ConsoleColor.Green);
                WriteColorLine("Get the following structures: \n" +
                               "Project\n" +
                               "The longest project task (by description)\n" +
                               "The shortest project task (by name)\n" +
                               "Total number of users in the project team, where either the project description > 20 characters or the number of tasks < 3", ConsoleColor.Yellow);

                var result = QueryService.Query7();

                WriteColorLine("Result: ", ConsoleColor.Yellow);
                WriteColorLine("For example for project with ID 5:", ConsoleColor.Yellow);
                WriteSuccessLine($"Project ID: {result[5].Project.Id} | Project Name: {result[5].Project.Name}");
                WriteSuccessLine($"The longest project task (by description): \n" +
                                 $"Task ID: {result[5].LongestProjectTaskByDescription.Id} | Task description length: {result[5].LongestProjectTaskByDescription.Description.Length}");
                WriteSuccessLine($"The shortest project task (by name): \n" +
                                 $"Task ID: {result[5].ShortestProjectTaskByName.Id} | Task name length: {result[5].ShortestProjectTaskByName.Name.Length}");
                WriteSuccessLine($"Total number of users in the project team: {result[5].UsersLenghtByCondition}\n");
                
                WriteColorLine("For example for project with ID 7:", ConsoleColor.Yellow);
                WriteSuccessLine($"Project ID: {result[7].Project.Id} | Project Name: {result[7].Project.Name}");
                WriteSuccessLine($"The longest project task (by description): \n" +
                                 $"Task ID: {result[7].LongestProjectTaskByDescription.Id} | Task description length: {result[7].LongestProjectTaskByDescription.Description.Length}");
                WriteSuccessLine($"The shortest project task (by name): \n" +
                                 $"Task ID: {result[7].ShortestProjectTaskByName.Id} | Task name length: {result[7].ShortestProjectTaskByName.Name.Length}");
                WriteSuccessLine($"Total number of users in the project team: {result[7].UsersLenghtByCondition}\n");
                
                WriteColorLine("For example for project with ID 10:", ConsoleColor.Yellow);
                WriteSuccessLine($"Project ID: {result[10].Project.Id} | Project Name: {result[10].Project.Name}");
                WriteSuccessLine($"The longest project task (by description): \n" +
                                 $"Task ID: {result[10].LongestProjectTaskByDescription.Id} | Task description length: {result[10].LongestProjectTaskByDescription.Description.Length}");
                WriteSuccessLine($"The shortest project task (by name): \n" +
                                 $"Task ID: {result[10].ShortestProjectTaskByName.Id} | Task name length: {result[10].ShortestProjectTaskByName.Name.Length}");
                WriteSuccessLine($"Total number of users in the project team: {result[10].UsersLenghtByCondition}\n");
                WriteEnterAnyKeyToContinue();
            }
            catch (Exception exception)
            {
                WriteErrorLine("Some error...");
                WriteErrorLine(exception.Message);
                WriteColorLine("You can look at my request in QueryService, maybe he doesn't work properly :(", ConsoleColor.Yellow);
                WriteEnterAnyKeyToContinue();
            }
        }
    }
}