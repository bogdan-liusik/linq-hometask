﻿namespace LinqHomeTask.ConsoleApp.Menu
{
    using System;

    public static class MenuHelper
    {
        public static void WriteColorLine(string @string, ConsoleColor color)
        {
            PrintColorLine(@string, color);
        }

        public static void WriteSuccessLine(string @string)
        {
            PrintColorLine(@string, ConsoleColor.Green);
        }

        public static void WriteErrorLine(string @string)
        {
            PrintColorLine(@string, ConsoleColor.Red);
        }

        public static void WriteEnterAnyKeyToContinue()
        {
            Console.WriteLine("Please, enter any key to continue...");
            Console.ReadKey();
        }
        
        private static void PrintColorLine(string @string, ConsoleColor color)
        {
            Console.ForegroundColor = color;
            Console.WriteLine(@string);
            Console.ResetColor();
        }
    }
}