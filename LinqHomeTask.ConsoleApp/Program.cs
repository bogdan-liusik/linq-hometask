﻿using LinqHomeTask.BLL.Services;
using Task = System.Threading.Tasks.Task;

namespace LinqHomeTask.ConsoleApp
{
    static class Program
    {
        static async Task Main(string[] args)
        {
            var dataService = new DataService();
            var data = await dataService.GetDataHierarchyAsync();
            var queryService = new QueryService(data);
            new Menu.Menu(queryService).Start();
        }
    }
}